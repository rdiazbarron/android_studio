package com.example.helloworld;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class BaseDatos extends SQLiteOpenHelper {
    //Escribir bien las instrucciones en sql
    String SQL_CREATE =" CREATE TABLE personas(id INTEGER PRIMARY KEY AUTOINCREMENT,ci VARCHAR(20), paterno VARCHAR(40),materno VARCHAR(40),nombres VARCHAR(60));";
    String SQL_DROP = "DROP TABLE IF EXISTS personas;";

    public BaseDatos(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DROP);
        db.execSQL(SQL_CREATE);
    }
}
