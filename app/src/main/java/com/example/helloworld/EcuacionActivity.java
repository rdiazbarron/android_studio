package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class EcuacionActivity extends AppCompatActivity {
    EditText editText1,editText2,editText3;
    TextView textView1,textView2;
    Button button1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ecuacion2);
        editText1 = findViewById(R.id.editText1);
        editText2 = findViewById(R.id.editText2);
        editText3 = findViewById(R.id.editText3);
        textView1 = findViewById(R.id.textView1);
        textView2 = findViewById(R.id.textView2);
        button1= findViewById(R.id.button1);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int a = Integer.parseInt(editText1.getText().toString());
                int b = Integer.parseInt(editText2.getText().toString());
                int c = Integer.parseInt(editText3.getText().toString());
                EcuacionClass miecuacion = new EcuacionClass(a,b,c);
                miecuacion.misolucion();
                textView1.setText(miecuacion.getX1());
                textView2.setText(miecuacion.getX2());
            }
        });

    }
}