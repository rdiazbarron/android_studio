package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MatricesActivity extends AppCompatActivity {
    Button buttonOperar;
    EditText editTamaño;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matrices);
        buttonOperar = findViewById(R.id.buttonOperar);
        editTamaño = findViewById(R.id.editTamaño);

        buttonOperar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int cantidad = Integer.parseInt(editTamaño.getText().toString());
                Intent intent = new Intent(MatricesActivity.this,MatricesActivity2.class);
                intent.putExtra("cantidad",cantidad);
                startActivity(intent);
            }
        });
    }
}