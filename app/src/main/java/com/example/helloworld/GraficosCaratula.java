package com.example.helloworld;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Typeface;
import android.view.View;

public class GraficosCaratula extends View {
    public GraficosCaratula(Context context) {
        super(context);
    }
//ctrol + o
    @Override

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //crear el fondo
        canvas.drawColor(Color.WHITE);
        //crear el pincel
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        Paint paintred = new Paint();
        paintred.setColor(Color.RED);
        paintred.setStrokeWidth(5);
        int ancho = getWidth();
        int alto = getHeight();
        //empieza arriba a la izq

        //canvas.drawLine(0,alto/2,ancho,alto/2,paint);
        //canvas.drawLine(ancho/2,0,ancho/2,alto,paint2);
        canvas.drawLine((float)(ancho*0.05),(float)(alto*0.05),(float)(ancho*0.05),(float)(alto*0.95),paintred);
        canvas.drawLine((float)(ancho*0.05),(float)(alto*0.05),(float)(ancho*0.95),(float)(alto*0.05),paintred);
        canvas.drawLine((float)(ancho*0.95),(float)(alto*0.05),(float)(ancho*0.95),(float)(alto*0.95),paintred);
        canvas.drawLine((float)(ancho*0.05),(float)(alto*0.95),(float)(ancho*0.95),(float)(alto*0.95),paintred);

        Paint paintCircle = new Paint();
        paintCircle.setColor(Color.BLUE);
        paintCircle.setStyle(Paint.Style.FILL);

        // Pincel para el texto
        Paint painttexto = new Paint();
        painttexto.setColor(Color.BLACK);
        painttexto.setTextSize(31);

        Paint painttexto2 = new Paint();
        painttexto2.setColor(Color.BLUE);
        painttexto2.setTextSize(31);

        Paint painttexto0 = new Paint();
        painttexto0.setColor(Color.RED);
        painttexto0.setTextSize(42);
        painttexto0.setShadowLayer(5, 2, 2, Color.GRAY);
        painttexto0.setStyle(Paint.Style.FILL_AND_STROKE);
        painttexto0.setStrokeWidth(1);
        painttexto0.setAntiAlias(true);
        painttexto0.setLetterSpacing(0.1f);

        canvas.drawText("Universidad San Francisco Xavier ", (float)(ancho*0.16), (float)(alto*0.14), painttexto0);
        canvas.drawText("de Chuquisaca ", (float)(ancho*0.35), (float)(alto*0.17), painttexto0);

        Paint painttexto01 = new Paint();
        painttexto01.setColor(Color.RED);
        painttexto01.setTextSize(42);


        Paint celular = new Paint();
        celular.setColor(Color.parseColor("#3F51B5"));
        canvas.drawRoundRect((float)(ancho*0.35), (float)(alto*0.25), (float)(ancho*0.60), (float)(alto*0.47), 80, 80, celular);

        Paint pantalla = new Paint();
        pantalla.setColor(Color.parseColor("#FFFFFF"));
        canvas.drawRoundRect((float)(ancho*0.38), (float)(alto*0.27), (float)(ancho*0.57), (float)(alto*0.44), 30, 30, pantalla);

        Paint boton = new Paint();
        boton.setColor(Color.parseColor("#4CAF50"));  // Verde
        canvas.drawRoundRect((float)(ancho*0.46), (float)(alto*0.45),(float)(ancho*0.49), (float)(alto*0.46), 10,10, boton);


        canvas.drawText("Carrera:", (float)(ancho*0.20), (float)(alto*0.55), painttexto2);
        canvas.drawText("Ing ciencias de la computacion", (float)(ancho*0.35), (float)(alto*0.55), painttexto);

        canvas.drawText("Nombres:", (float)(ancho*0.20), (float)(alto*0.58), painttexto2);
        canvas.drawText("Ricardo Jairo Diaz Barron", (float)(ancho*0.35), (float)(alto*0.58), painttexto);

        canvas.drawText("Materia:", (float)(ancho*0.20), (float)(alto*0.61), painttexto2);
        canvas.drawText("Desarrollo movil", (float)(ancho*0.35), (float)(alto*0.61), painttexto);

        canvas.drawText("Docente:", (float)(ancho*0.20), (float)(alto*0.64), painttexto2);
        canvas.drawText("Ing. Oswaldo Velasquez", (float)(ancho*0.35), (float)(alto*0.64), painttexto);

        canvas.drawText("Carnet Univ:", (float)(ancho*0.20), (float)(alto*0.67), painttexto2);
        canvas.drawText("111-349", (float)(ancho*0.37), (float)(alto*0.67), painttexto);

        Paint ovalPaint = new Paint();
        ovalPaint.setColor(Color.parseColor("#55FFA07A"));  // Rojo translúcido
        canvas.drawRoundRect((float)(ancho*0.18), (float)(alto*0.53), (float)(ancho*0.8), (float)(alto*0.70), 30,30,ovalPaint);

        //Paint linePaint = new Paint();
        //linePaint.setColor(Color.parseColor("#3F51B5"));  // Azul
        //linePaint.setStrokeWidth(5);
        //canvas.drawLine((float)(ancho*0.2), (float)(alto*0.47), (float)(ancho*0.6), (float)(alto*0.47), linePaint);



   //     Paint rectPaint = new Paint();
        //rectPaint.setColor(Color.parseColor("#E0E0E0"));  // Gris
        //canvas.drawRoundRect((float)(ancho*0.06), (float)(alto*0.12), (float)(ancho*0.8), (float)(alto*0.2), 20, 20, rectPaint);

        canvas.drawText("Sucre - Bolivia ", (float)(ancho*0.38), (float)(alto*0.84), painttexto01);
        canvas.drawText("2023 ", (float)(ancho*0.45), (float)(alto*0.87), painttexto01);



        // Dibujo del fondo azul del logo
        int screenWidth = canvas.getWidth();
        int screenHeight = canvas.getHeight();

        // Dimensiones del logo
        float logoWidth = 200;  // Por ejemplo, 400 - 100 = 300
        float logoHeight = 200; // Igualmente, 400 - 100 = 300

        // Calculamos las coordenadas para que el logo quede centrado
        float left = (screenWidth - logoWidth) / 2 -25;
        float top = (screenHeight - logoHeight) / 2 -250;
        float right = left + logoWidth;
        float bottom = top + logoHeight;

        Paint paint5 = new Paint();
        paint5.setColor(Color.parseColor("#1877F2"));  // El color azul clásico de Facebook
        canvas.drawRect(left, top, right, bottom, paint5);

        // Centramos el texto en el rectángulo
        paint5.setColor(Color.WHITE);
        paint5.setTextSize(180);
        paint5.setTextAlign(Paint.Align.CENTER);
        paint5.setAntiAlias(true);

        // Para centrar verticalmente el texto, calculamos la posición vertical
        Paint.FontMetrics fontMetrics = paint5.getFontMetrics();
        float textHeight = fontMetrics.bottom - fontMetrics.top;
        float textOffset = (textHeight / 2) - fontMetrics.bottom;

        float x = screenWidth / 2 -25;  // Esto coloca el centro del texto en el centro del canvas
        float y = (screenHeight / 2) + textOffset -250;

        canvas.drawText("f", x, y, paint5);




    }

    // Esta función auxiliar genera un Path a partir de puntos dados.



    }

