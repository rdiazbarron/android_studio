package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.Toast;

public class MatricesActivity2 extends AppCompatActivity {
    GridLayout gridMatrix1, gridMatrix2;
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matrices2);

        Intent intent = getIntent();
        int cantidad = intent.getIntExtra("cantidad",3);
        Toast.makeText(MatricesActivity2.this,"Resultado:"+cantidad,Toast.LENGTH_SHORT).show();

        gridMatrix1 = findViewById(R.id.gridMatrix1);
        generateMatrix(gridMatrix1, cantidad);

        gridMatrix2 = findViewById(R.id.gridMatrix2);
        generateMatrix(gridMatrix2, cantidad);
    }

    private void generateMatrix(GridLayout grid, int cantidad) {
        grid.setColumnCount(cantidad);
        for (int i = 0; i < cantidad * cantidad; i++) {
            EditText editText = new EditText(this);
            GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams();
            layoutParams.width = 0;
            layoutParams.height = GridLayout.LayoutParams.WRAP_CONTENT;
            layoutParams.columnSpec = GridLayout.spec(GridLayout.UNDEFINED, 1f);
            editText.setLayoutParams(layoutParams);
            grid.addView(editText);
        }
    }
}