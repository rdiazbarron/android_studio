package com.example.helloworld;

public class NumerosImagCalc {
    public float a,b,c,d;
    public String resfinal;

    public String getResfinal() {
        return resfinal;
    }

    public float getA() {
        return a;
    }

    public void setA(float a) {
        this.a = a;
    }

    public float getB() {
        return b;
    }

    public void setB(float b) {
        this.b = b;
    }

    public float getC() {
        return c;
    }

    public void setC(float c) {
        this.c = c;
    }

    public float getD() {
        return d;
    }

    public void setD(float d) {
        this.d = d;
    }

    public NumerosImagCalc(float a, float b, float c, float d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }
    public void Adicion(){
        String resreal = String.valueOf(this.a + this.c);
        String resimag = String.valueOf(this.b + this.d);
        resfinal = resreal + '+' + resimag + 'i';
    }
    public void Resta(){
        String resreal = String.valueOf(this.a - this.c);
        String resimag = String.valueOf(this.b - this.d);
        resfinal = resreal + '-' + resimag + 'i';
    }
    public void Multiplicar() {
        String resparcial1 = String.valueOf((this.a * this.c)-(this.b * this.d));
        String resparcial2 =  String.valueOf((this.a*this.d)+(this.b*this.c));
        resfinal = resparcial1 + "+" + resparcial2 + "i";
    }

    public void Division() {
        String resparcial1 = String.valueOf((this.a*this.c + this.b*this.d)/(this.c*this.c + this.d*this.d)) ;
        String resparcial2 = String.valueOf((this.b *this.c - this.a *this.d)/(this.c*this.c + this.d*this.d));
        resfinal = resparcial1 + "+" + resparcial2 + "i";
    }
}
