package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ecuacion extends AppCompatActivity {
    TextView textView1,textView2;
    EditText editText1,editText2,editText3;
    Button button1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ecuacion);
        editText1 = findViewById(R.id.editText1);
        editText2 = findViewById(R.id.editText2);
        editText3 = findViewById(R.id.editText3);
        button1 = findViewById(R.id.button1);
        textView1 = findViewById(R.id.textView1);
        textView2 = findViewById(R.id.textView2);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float a = Float.parseFloat(editText1.getText().toString());
                float b = Float.parseFloat(editText2.getText().toString());
                float c = Float.parseFloat(editText3.getText().toString());
                calculoEcuacion calc = new calculoEcuacion(a,b,c);
                calc.solution();
                textView1.setText(calc.getX1());
                textView2.setText(calc.getX1());

            }
        });
    }

}