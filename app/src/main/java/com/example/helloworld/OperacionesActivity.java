package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class OperacionesActivity extends AppCompatActivity {
    EditText editText1, editText2;
    Button buttonSuma;
    TextView textViewResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_operaciones);

        editText1 = findViewById(R.id.editText1);
        editText2 = findViewById(R.id.editText2);
        buttonSuma = findViewById(R.id.buttonSuma);
        textViewResultado = findViewById(R.id.textViewResultado);

        buttonSuma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                int x = Integer.parseInt(editText1.getText().toString());
                int y = Integer.parseInt(editText2.getText().toString());
                Operaciones operaciones = new Operaciones(x,y);
                //int z = x + y;
                textViewResultado.setText(String.valueOf(operaciones.sumar()));
            }
        });
        buttonSuma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }
}