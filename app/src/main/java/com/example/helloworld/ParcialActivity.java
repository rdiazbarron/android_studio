package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ParcialActivity extends AppCompatActivity {
    Button buttonPreg1,buttonPreg2,buttonPreg3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_parcial);
        buttonPreg1 = findViewById(R.id.buttonPreg1);
        buttonPreg2 = findViewById(R.id.buttonPreg2);
        buttonPreg3 = findViewById(R.id.buttonPreg3);

        buttonPreg1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ParcialActivity.this, Preg1Activity.class);
                startActivity(intent);
            }
        });

        buttonPreg2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ParcialActivity.this, Preg2Activity.class);
                startActivity(intent);
            }
        });

        buttonPreg3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setContentView(new Figuras2(ParcialActivity.this));

            }
        });

    }
}