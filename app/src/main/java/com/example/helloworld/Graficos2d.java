package com.example.helloworld;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.View;

public class Graficos2d extends View {
    //en terminal con objeivo de hacer un trace
    private static final String TAG = "DATOS";

    public Graficos2d(Context context) {
        super(context);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //FONDO BLANCO
        canvas.drawColor(Color.BLACK);
        //ahora lapiz
        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        int ancho = getWidth();
        int alto = getHeight();
        Log.d(TAG,"Ancho = "+ancho+ "Alto="+alto);
        //canvas.drawLine(10,10,100,100,paint);
        canvas.drawLine(0,alto/2,ancho,alto/2,paint);
        canvas.drawLine(ancho/2,0,ancho/2,alto,paint);
        //ancho 1080, alto =1992
        float limInfx = -20;
        float limSupx = 20;
        float limInfy = -20;
        float limSupy = 20;
        for(float x = limInfx; x<=limSupx;x+=0.01){
            double y = fx(x);
            //Log.d(TAG, "x="+x+"y="+y);
            float xt = ((x - limInfx)/(limSupx - limInfx))*ancho;
            Log.d(TAG,"x="+x+"xt="+xt);
            double yt = alto - (((y - limInfy)/(limSupy - limInfy))*alto);
            canvas.drawCircle((float)xt,(float)yt,5,paint);
            Log.d(TAG,"y="+y+"xt="+yt);
        }

    }
    double fx(float x){
        //return Math.cos(x);
        //return Math.sin(x);
        //return Math.tan(x);
        return Math.atan(x)*x;
    }
}
