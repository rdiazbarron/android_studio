package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PostActivity extends AppCompatActivity {

    EditText editNombre22, editCarnet22, editPaterno22, editMaterno22, textId22;
    Button buttonPosti, buttonPut, buttonDel, buttonRead;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);
        editNombre22 = findViewById(R.id.editNombres22);
        editPaterno22 = findViewById(R.id.editPaterno22);
        editMaterno22 = findViewById(R.id.editMaterno22);
        editCarnet22 = findViewById(R.id.editCarnet22);
        buttonPosti = findViewById(R.id.buttonPosti);
        buttonPut = findViewById(R.id.buttonPut);
        textId22 = findViewById(R.id.textId22);
        buttonDel = findViewById(R.id.buttonDel);
        buttonRead = findViewById(R.id.buttonRead);

        buttonPosti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postPersona();
            }
        });

        buttonPut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                putPersona();
            }
        });

        buttonDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delPersona();
            }
        });
        buttonRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rePersona();
            }
        });
    }

    public void postPersona(){
        String nombre = editNombre22.getText().toString();
        String paterno = editPaterno22.getText().toString();
        String materno = editMaterno22.getText().toString();
        String ci = editCarnet22.getText().toString();

        String URL_WS = "http://192.168.0.19/movil/";
        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL_WS)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        WebServiceAPI webServiceApi = retrofit.create(WebServiceAPI.class);
        Call<WSPersona> personaCall = webServiceApi.postPersona(new WSPersona(paterno,materno,nombre,ci));
        personaCall.enqueue(new Callback<WSPersona>() {
            @Override
            public void onResponse(Call<WSPersona> call, Response<WSPersona> response) {
                if (!response.isSuccessful()) {
                    String error = "Ha ocurrido un error. Contacte al administrador";
                    if (response.errorBody()
                            .contentType()
                            .subtype()
                            .equals("json")) {
                    } else {
                        try {
                            Log.d("LoginActivity", response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    return;
                }
            }

            @Override
            public void onFailure(Call<WSPersona> call, Throwable t) {
                Log.e("RetrofitError", t.getMessage());
                Toast.makeText(PostActivity.this, "Error en la actualización", Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void putPersona(){
        int id = Integer.parseInt(textId22.getText().toString());
        String nombre = editNombre22.getText().toString();
        String paterno = editPaterno22.getText().toString();
        String materno = editMaterno22.getText().toString();
        String ci = editCarnet22.getText().toString();

        String URL_WS = "http://192.168.0.19/movil/";
        Gson gson = new GsonBuilder().setLenient().create();
        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL_WS)

                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        WebServiceAPI webServiceApi = retrofit.create(WebServiceAPI.class);
        Call<WSPersona> personaCall = webServiceApi.updatePersona(id, new WSPersona(paterno,materno,nombre,ci,id));
        personaCall.enqueue(new Callback<WSPersona>() {
            @Override
            public void onResponse(Call<WSPersona> call, Response<WSPersona> response) {
                if (!response.isSuccessful()) {
                    String error = "Ha ocurrido un error. Contacte al administrador";
                    if (response.errorBody()
                            .contentType()
                            .subtype()
                            .equals("json")) {
                    } else {
                        try {
                            Log.d("UpdateActivity", response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    return;
                }
                Toast.makeText(PostActivity.this, "Actualización exitosa", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<WSPersona> call, Throwable t) {
                Log.e("RetrofitError", t.getMessage());
                Toast.makeText(PostActivity.this, "Error en la actualización", Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void delPersona(){
        int id = Integer.parseInt(textId22.getText().toString());
        String URL_WS = "http://192.168.0.19/movil/";
        Gson gson = new GsonBuilder().setLenient().create();
        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL_WS)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        WebServiceAPI webServiceApi = retrofit.create(WebServiceAPI.class);
        Call<Void> deleteCall = webServiceApi.deletePersona(id);

        deleteCall.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(PostActivity.this, "Eliminación exitosa", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(PostActivity.this, "Error al eliminar", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.e("RetrofitError", t.getMessage());
                Toast.makeText(PostActivity.this, "Error en la solicitud", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void rePersona() {
        int id = Integer.parseInt(textId22.getText().toString());

        String URL_WS = "http://192.168.0.19/movil/";
        Gson gson = new GsonBuilder().setLenient().create();
        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL_WS)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        WebServiceAPI webServiceApi = retrofit.create(WebServiceAPI.class);
        Call<WSPersona> personaCall = webServiceApi.readPersona(id);

        personaCall.enqueue(new Callback<WSPersona>() {
            @Override
            public void onResponse(Call<WSPersona> call, Response<WSPersona> response) {
                if (response.isSuccessful()) {
                    WSPersona persona = response.body();
                    // Aquí puedes mostrar los datos de la persona en tu interfaz
                    // Por ejemplo:
                    editNombre22.setText(persona.getNombres());
                    editPaterno22.setText(persona.getPaterno());
                    editMaterno22.setText(persona.getMaterno());
                    editCarnet22.setText(persona.getCi());
                } else {
                    Toast.makeText(PostActivity.this, "Error al obtener detalles", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<WSPersona> call, Throwable t) {
                Log.e("RetrofitError", t.getMessage());
                Toast.makeText(PostActivity.this, "Error en la conexión", Toast.LENGTH_SHORT).show();
            }
        });
    }


}