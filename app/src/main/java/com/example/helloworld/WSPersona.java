package com.example.helloworld;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class WSPersona {

    @SerializedName("paterno")
    @Expose
    private String paterno;
    @SerializedName("materno")
    @Expose
    private String materno;
    @SerializedName("nombres")
    @Expose
    private String nombres;
    @SerializedName("ci")
    @Expose
    private String ci;

    @SerializedName("id")
    @Expose
    private int id;


    public WSPersona(String paterno, String materno, String nombres, String ci) {
        this.paterno = paterno;
        this.materno = materno;
        this.nombres = nombres;
        this.ci = ci;
    }

    public WSPersona(String paterno, String materno, String nombres, String ci, int id) {
        this(paterno, materno, nombres, ci);  // Llama al otro constructor
        this.id = id;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPaterno() {
        return paterno;
    }

    public void setPaterno(String paterno) {
        this.paterno = paterno;
    }

    public String getMaterno() {
        return materno;
    }

    public void setMaterno(String materno) {
        this.materno = materno;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getCi() {
        return ci;
    }

    public void setCi(String ci) {
        this.ci = ci;
    }

}