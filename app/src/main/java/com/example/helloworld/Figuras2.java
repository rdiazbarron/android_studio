package com.example.helloworld;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

public class Figuras2 extends View {
    public Figuras2(Context context) { super(context); }
    @Override

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawColor(Color.WHITE);


        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setStrokeWidth(2);

        int ancho = getWidth();
        int alto = getHeight();

        int radioInicial = 10000;

        int limite = ancho - 20;
        int radioFinal = radioInicial;

        if (2 * radioInicial > limite) {
            radioFinal = limite / 2;
        }

        float centroX = ancho / 2.0f;
        float centroY = alto / 2.0f;
        canvas.drawCircle(centroX, centroY, radioFinal, paint);
    }
}
