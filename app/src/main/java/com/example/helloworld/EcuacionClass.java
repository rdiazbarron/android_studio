package com.example.helloworld;

public class EcuacionClass {
    public int a;
    public int b;
    public int c;
    public String x1;
    public String x2;

    public EcuacionClass(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public String getX1() {
        return x1;
    }

    public String getX2() {
        return x2;
    }

    public float d(){

        return (float)(this.b*this.b)-(4*this.a*this.c);
    }
    public void misolucion(){
        if(this.d()==0){
            this.x1 = String.valueOf(-this.b/(2*this.a));
            this.x2 = String.valueOf(-this.b/(2*this.a));
        }else if(this.d()>0){
            this.x1 = String.valueOf((-this.b + Math.sqrt(this.d()))/(2*this.a));
            this.x2 = String.valueOf((-this.b - Math.sqrt(this.d()))/(2*this.a));
        }else{
           String parte1 = String.valueOf((-this.b/(2*this.a)));
           String parte2 = String.valueOf(Math.sqrt(Math.abs(this.d()))/(2*this.a));
           this.x1 = parte1 + '+' + parte2 + 'i';
           this.x2 = parte1 + '-' + parte2 + 'i';
        }
    }
}
