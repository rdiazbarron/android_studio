package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import io.reactivex.schedulers.Schedulers;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import io.reactivex.functions.Function;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.springframework.util.ObjectUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import io.reactivex.Observable;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import io.reactivex.Observable;

import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MainActivity extends AppCompatActivity {
    Button buttonMatrices, buttonRX, buttonArqui, buttonOmega, buttonParcial1, buttonOperaciones, buttonPost, buttonConsumir, buttonBD, buttonElix, buttonSalir, buttonSecond, buttonImagi, buttonSecondGrade, buttonCalc, buttongraficos2d, buttonCaratula, buttonFiguras, buttonEnviar;
    BaseDatos baseDatos;
    SQLiteDatabase db;
    List<WSAlbums> listData = new ArrayList<>();
    List<WSPersons> listDataPersons = new ArrayList<>();

    List<WSCompras> listDataCompras = new ArrayList<>();


    private List<WSPersons> listDataPersons2 = new ArrayList<>();
    private List<WSCompras> listDataCompras2 = new ArrayList<>();

    private List<WSVentas> listDataVentas2 = new ArrayList<>();
    private List<WSReserva> listDataReserva2 = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonOmega = findViewById(R.id.buttonOmega);
        buttonMatrices = findViewById(R.id.buttonMatrices);
        buttonPost = findViewById(R.id.buttonPost);
        buttonRX = findViewById(R.id.buttonRX);
        buttonBD = findViewById(R.id.buttonBD);
        buttonOperaciones = findViewById(R.id.buttonOperaciones);
        buttonSalir = findViewById(R.id.buttonSalir);
        buttonSecond = findViewById(R.id.buttonSecond);
        buttonImagi = findViewById(R.id.buttonImagi);
        buttonElix = findViewById(R.id.buttonElix);
        buttonParcial1 = findViewById(R.id.buttonParcial1);
        buttonEnviar = findViewById(R.id.buttonEnviar);

        buttonSecondGrade = findViewById(R.id.buttonSecondGrade);
        buttonCalc = findViewById(R.id.buttonCalc);
        buttonArqui = findViewById(R.id.buttonArqui);
        buttongraficos2d = findViewById(R.id.buttongraficos2d);
        buttonCaratula = findViewById(R.id.buttonCaratula);
        buttonFiguras = findViewById(R.id.buttonFiguras);
        buttonConsumir = findViewById(R.id.buttonConsumir);

        buttonArqui.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ArquiActivity.class);
                startActivity(intent);
            }
        });
        buttonOmega.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, OmegaActivity.class);
                startActivity(intent);
            }
        });
        buttonParcial1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ParcialActivity.class);
                startActivity(intent);
            }
        });

        /* baseDatos = new BaseDatos(MainActivity.this,"BDPersonas",null,1);*/
        buttonMatrices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, MatricesActivity.class);
                startActivity(intent);
            }
        });


        buttonBD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, BDActivity.class);
                startActivity(intent);
            }
        });


        buttonFiguras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setContentView(new Figuras(MainActivity.this));

            }
        });


        //OPENGL
        buttonCaratula.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setContentView(new GraficosCaratula(MainActivity.this));
            }
        });


        buttongraficos2d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setContentView(new Graficos2d(MainActivity.this));
            }
        });


        buttonCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, CalculadoranActivity.class);
                startActivity(intent);

            }
        });
        buttonOperaciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, OperacionesActivity.class);
                startActivity(intent);

            }
        });

        buttonImagi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, NumerosImag.class);
                startActivity(intent);

            }
        });

        buttonSecond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ecuacion.class);
                startActivity(intent);

            }
        });
        buttonElix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, EcuacionActivity.class);
                startActivity(intent);

            }
        });

        buttonSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        buttonConsumir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDataCompras();
            }
        });

        buttonRX.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recoveryData();
            }
        });

//        buttonPost.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) { postPersona();
//            }
//        });

        buttonPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, PostActivity.class);
                startActivity(intent);

            }
        });

        buttonEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Intent intent = new Intent(MainActivity.this, PruebaActivity.class);
//                intent.putExtra("mi cadena","Hola a todos");
//                intent.putExtra("mi entero",70);
//                startActivity(intent);


                Oper oper = new Oper(15, 14);
                Intent intent2 = new Intent(MainActivity.this, PruebaActivity.class);
                intent2.putExtra("Miobjeto", oper);
                startActivity(intent2);//enviando objeto serializado
//
//
//                db = baseDatos.getWritableDatabase();
//                db.execSQL("INSERT INTO personas(ci,paterno,materno,nombres)VALUES('5663818','PEREZ','LOZANO','JUAN');");
//                String SQL = "SELECT * from personas;";
//                //podriamos usar toast aqui- esto es basico
//                db = baseDatos.getReadableDatabase();//concepto de cursor
//                Cursor cursor = db.rawQuery(SQL,null);
//                if (cursor.moveToFirst()){
//                    do{
//                        String paterno = cursor.getString(3);
//                        Toast.makeText(MainActivity.this,paterno,Toast.LENGTH_SHORT).show();
//                    }while(cursor.moveToNext());
//                }


            }
        });


        buttonSecondGrade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, EcuacionActivity.class);
                startActivity(intent);

            }
        });

    }

    public void getDataPersons() {
        String URL_WS = "http://192.168.0.19/movil/";
        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL_WS)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        WebServiceAPI webServiceApi = retrofit.create(WebServiceAPI.class);
        final Call<List<WSPersons>> call = webServiceApi.getPersons();
        call.enqueue(new Callback<List<WSPersons>>() {
            @Override
            public void onResponse(Call<List<WSPersons>> call, Response<List<WSPersons>> response) {
                if (response.isSuccessful()) {
                    listDataPersons = response.body();
                    Toast.makeText(MainActivity.this, listDataPersons.get(1).getPaterno(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, "REVISE SU SERVICIO DE INTERNET", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<WSPersons>> call, Throwable t) {
                Toast.makeText(MainActivity.this, "REVISE EL SERVICIO WEB DE INTERNET", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getDataCompras() {
        String URL_WS = "http://192.168.78.117/movil/";
        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL_WS)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        WebServiceAPI webServiceApi = retrofit.create(WebServiceAPI.class);
        final Call<List<WSCompras>> call = webServiceApi.getCompras();
        call.enqueue(new Callback<List<WSCompras>>() {
            @Override
            public void onResponse(Call<List<WSCompras>> call, Response<List<WSCompras>> response) {
                if (response.isSuccessful()) {
                    listDataCompras = response.body();
                    Toast.makeText(MainActivity.this, listDataCompras.get(0).getDescripcion(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, "REVISE SU SERVICIO DE INTERNET", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<WSCompras>> call, Throwable t) {
                Log.e("RetrofitError", t.getMessage());
                Toast.makeText(MainActivity.this, "REVISE EL SERVICIO WEB DE INTERNET", Toast.LENGTH_SHORT).show();
            }
        });
    }

    //    public void postPersona(){
//        String URL_WS = "http://192.168.78.117/movil/";
//        final Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl(URL_WS)
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//        WebServiceAPI webServiceApi = retrofit.create(WebServiceAPI.class);
//        Call<WSPersona> personaCall = webServiceApi.postPersona(new WSPersona("a","b","c","123"));
//        personaCall.enqueue(new Callback<WSPersona>() {
//            @Override
//            public void onResponse(Call<WSPersona> call, Response<WSPersona> response) {
//                if (!response.isSuccessful()) {
//                    String error = "Ha ocurrido un error. Contacte al administrador";
//                    if (response.errorBody()
//                            .contentType()
//                            .subtype()
//                            .equals("json")) {
//                    } else {
//                        try {
//                            Log.d("LoginActivity", response.errorBody().string());
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                    return;
//                }
//            }
//
//            @Override
//            public void onFailure(Call<WSPersona> call, Throwable t) {
//
//            }
//        });
////    }
    public void getData() {
        String URL_WS = "http://192.168.78.117/movil/";
        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL_WS)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        WebServiceAPI webServiceApi = retrofit.create(WebServiceAPI.class);
        final Call<List<WSAlbums>> call = webServiceApi.getAlbums();
        call.enqueue(new Callback<List<WSAlbums>>() {//se ejecuta en segundo plano
            @Override
            public void onResponse(Call<List<WSAlbums>> call, Response<List<WSAlbums>> response) {
                if (response.isSuccessful()) {
                    listData = response.body();
                    Toast.makeText(MainActivity.this, listData.get(3).getTitle(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, "REVISE SU SERVICIO DE INTERNET", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<WSAlbums>> call, Throwable t) {
                Toast.makeText(MainActivity.this, "REVISE EL SERVICIO WEB DE INTERNET", Toast.LENGTH_SHORT).show();
            }
        });


    }

    private void recoveryData() {
        String URL_WS = "http://192.168.0.11/movil/";
        Gson gson = new GsonBuilder().setLenient().create();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit.Builder builder = new Retrofit.Builder();
        builder.baseUrl(URL_WS);
        builder.client(client);
        builder.addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        builder.addConverterFactory(GsonConverterFactory.create(gson));
        Retrofit retrofit2 = builder.build();
        WebServiceAPI webServiceApi2 = retrofit2.create(WebServiceAPI.class);
        List<Observable<?>> requests = new ArrayList<>();

        requests.add(webServiceApi2.getPersonsObs());
        requests.add(webServiceApi2.getComprasObs());
        requests.add(webServiceApi2.getVentasObs());
        requests.add(webServiceApi2.getReservaObs());
        Disposable obs = Observable.zip(requests, new Function<Object[], Object>() {
                    @Override
                    public Object apply(Object[] objects) throws Exception {
                        listDataPersons2 = (List<WSPersons>) objects[0];
                        listDataCompras2 = (List<WSCompras>) objects[1];
                        listDataVentas2 = (List<WSVentas>) objects[2];
                        listDataReserva2 = (List<WSReserva>) objects[3];
                        return new Object();
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(
                        new DisposableObserver<Object>() {
                            @Override
                            public void onNext(Object o) {
                                if (listDataPersons2 != null && !listDataPersons2.isEmpty()) {
                                    String paterno = listDataPersons2.get(0).getPaterno(); // Asumiendo que tu clase WSPersons tiene un método getName()
                                    Toast.makeText(MainActivity.this, paterno, Toast.LENGTH_SHORT).show();
                                }

                                if (listDataCompras2 != null && !listDataCompras2.isEmpty()) {
                                    String descripcion = listDataCompras2.get(0).getDescripcion();  // Suponiendo que WSCompras tiene un método getDetail()
                                    Toast.makeText(MainActivity.this, descripcion, Toast.LENGTH_SHORT).show();
                                }

                                if (listDataVentas2 != null && !listDataVentas2.isEmpty()) {
                                    String venta = listDataVentas2.get(0).getDescripcion();  // Suponiendo que WSCompras tiene un método getDetail()
                                    Toast.makeText(MainActivity.this, venta, Toast.LENGTH_SHORT).show();
                                }

                                if (listDataReserva2 != null && !listDataReserva2.isEmpty()) {
                                    String reserva = listDataReserva2.get(0).getItem();  // Suponiendo que WSCompras tiene un método getDetail()
                                    Toast.makeText(MainActivity.this, reserva, Toast.LENGTH_SHORT).show();
                                }
                            }
                            @Override
                            public void onError(Throwable e) {
                                Log.d("Datos", "OnError ");
                            }
                            @Override
                            public void onComplete() {
                                Log.d("Datos", "Finalizado con exito onComplete ");
                            }
                        });
    }
}

