package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

public class PruebaActivity extends AppCompatActivity {
    //comunicacion entre activitis de forma autonoma

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prueba);

//        Intent intent = getIntent();
//        String dato1 = intent.getStringExtra("mi cadena");
//        int dato2 = intent.getIntExtra("mi entero",0);
//        Toast.makeText(PruebaActivity.this,dato1 +""+dato2, Toast.LENGTH_SHORT).show();


        Intent intent2 = getIntent();
        Oper oper2 = (Oper)intent2.getSerializableExtra("Miobjeto");
        Toast.makeText(PruebaActivity.this,"Resultado:"+oper2.suma(),Toast.LENGTH_SHORT).show();
//
//

        //sistemas multiagente con java y jade

    }
}