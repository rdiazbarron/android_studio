package com.example.helloworld;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class BaseDatos3 extends SQLiteOpenHelper {

    /*String SQL_DROP = "DROP TABLE IF EXISTS personas;";*/

    public BaseDatos3(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String SQL_CREATE ="CREATE TABLE personas(id INTEGER PRIMARY KEY AUTOINCREMENT,ci VARCHAR(20), paterno VARCHAR(40),materno VARCHAR(40),nombres VARCHAR(60));";
        db.execSQL(SQL_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS personas");
        onCreate(db);
    }
}
