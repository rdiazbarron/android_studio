package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class NumerosImag extends AppCompatActivity {
    EditText editTextNumA,editTextNumB,editTextNumC,editTextNumD;
    Button buttonAdicion,buttonResta,buttonMultiplicacion,buttonDivision;
    TextView textViewIgual;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_numeros_imag);
        editTextNumA = findViewById(R.id.editTextNumA);
        editTextNumB = findViewById(R.id.editTextNumB);
        editTextNumC = findViewById(R.id.editTextNumC);
        editTextNumD = findViewById(R.id.editTextNumD);
        buttonAdicion = findViewById(R.id.buttonAdicion);
        buttonResta = findViewById(R.id.buttonResta);
        buttonMultiplicacion = findViewById(R.id.buttonMultiplicacion);
        buttonDivision = findViewById(R.id.buttonDivision);
        textViewIgual = findViewById(R.id.textViewIgual);

        buttonAdicion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float a = Float.parseFloat(editTextNumA.getText().toString());
                float b = Float.parseFloat(editTextNumB.getText().toString());
                float c = Float.parseFloat(editTextNumC.getText().toString());
                float d = Float.parseFloat(editTextNumD.getText().toString());
                NumerosImagCalc NumerosImagCalc= new NumerosImagCalc(a,b,c,d);
                NumerosImagCalc.Adicion();
                textViewIgual.setText(NumerosImagCalc.getResfinal());
            }
        });

        buttonResta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float a = Float.parseFloat(editTextNumA.getText().toString());
                float b = Float.parseFloat(editTextNumB.getText().toString());
                float c = Float.parseFloat(editTextNumC.getText().toString());
                float d = Float.parseFloat(editTextNumD.getText().toString());
                NumerosImagCalc NumerosImagCalc= new NumerosImagCalc(a,b,c,d);
                NumerosImagCalc.Resta();
                textViewIgual.setText(NumerosImagCalc.getResfinal());
            }
        });

        buttonMultiplicacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float a = Float.parseFloat(editTextNumA.getText().toString());
                float b = Float.parseFloat(editTextNumB.getText().toString());
                float c = Float.parseFloat(editTextNumC.getText().toString());
                float d = Float.parseFloat(editTextNumD.getText().toString());
                NumerosImagCalc NumerosImagCalc= new NumerosImagCalc(a,b,c,d);
                NumerosImagCalc.Multiplicar();
                textViewIgual.setText(NumerosImagCalc.getResfinal());
            }
        });
        buttonDivision.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float a = Float.parseFloat(editTextNumA.getText().toString());
                float b = Float.parseFloat(editTextNumB.getText().toString());
                float c = Float.parseFloat(editTextNumC.getText().toString());
                float d = Float.parseFloat(editTextNumD.getText().toString());
                NumerosImagCalc NumerosImagCalc= new NumerosImagCalc(a,b,c,d);
                NumerosImagCalc.Division();
                textViewIgual.setText(NumerosImagCalc.getResfinal());
            }
        });
    }
}