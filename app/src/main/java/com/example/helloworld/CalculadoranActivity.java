package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.service.autofill.TextValueSanitizer;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CalculadoranActivity extends AppCompatActivity {
    Button button1,button2,button3,button4,button5,button6,button7,button8,button9,button0, buttonclear,buttonplus,buttonless,buttonproduct,buttondiv,buttonequal;
    TextView textViewPantalla,textViewPantalla2;
    String acumulado1 ="";
    int operacion,valor1,valor2,valorfinal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadoran);
        button1 = findViewById(R.id.button1);
        button2 = findViewById(R.id.button2);
        button3 = findViewById(R.id.button3);
        button4 = findViewById(R.id.button4);
        button5= findViewById(R.id.button5);
        button6 = findViewById(R.id.button6);
        button7 = findViewById(R.id.button7);
        button8 = findViewById(R.id.button8);
        button9 = findViewById(R.id.button9);
        button0 = findViewById(R.id.button0);
        buttonplus = findViewById(R.id.buttonplus);
        buttonless= findViewById(R.id.buttonless);
        buttonproduct = findViewById(R.id.buttonproduct);
        buttonequal=findViewById(R.id.buttonequal);
        buttondiv=findViewById(R.id.buttondiv);
        buttonclear=findViewById(R.id.buttonclear);
        textViewPantalla=findViewById(R.id.textViewPantalla);
        textViewPantalla2=findViewById(R.id.textViewPantalla2);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                acumulado1 = acumulado1 + (button1.getText().toString());

                textViewPantalla.setText(acumulado1);
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                acumulado1 = acumulado1 + (button2.getText().toString());

                textViewPantalla.setText(acumulado1);
            }
        });
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                acumulado1 = acumulado1 + (button3.getText().toString());

                textViewPantalla.setText(acumulado1);
            }
        });
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                acumulado1 = acumulado1 + (button4.getText().toString());

                textViewPantalla.setText(acumulado1);
            }
        });
        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                acumulado1 = acumulado1 + (button5.getText().toString());

                textViewPantalla.setText(acumulado1);
            }
        });
        button6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                acumulado1 = acumulado1 + (button6.getText().toString());

                textViewPantalla.setText(acumulado1);
            }
        });
        button7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                acumulado1 = acumulado1 + (button7.getText().toString());

                textViewPantalla.setText(acumulado1);
            }
        });
        button8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                acumulado1 = acumulado1 + (button8.getText().toString());

                textViewPantalla.setText(acumulado1);
            }
        });
        button9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                acumulado1 = acumulado1 + (button9.getText().toString());

                textViewPantalla.setText(acumulado1);
            }
        });
        button0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                acumulado1 = acumulado1 + (button0.getText().toString());

                textViewPantalla.setText(acumulado1);
            }
        });

        buttonclear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                acumulado1 = "";
                textViewPantalla.setText(acumulado1);
                textViewPantalla2.setText(acumulado1);
            }
        });



        buttonplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                valor1 = Integer.parseInt(acumulado1);
                textViewPantalla2.setText(acumulado1);
                acumulado1 = "";
                textViewPantalla.setText(acumulado1);
                operacion = 1;
            }
        });

        buttonless.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                valor1 = Integer.parseInt(acumulado1);
                textViewPantalla2.setText(acumulado1);
                acumulado1 = "";
                textViewPantalla.setText(acumulado1);
                operacion = 2;
            }
        });
        buttonproduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                valor1 = Integer.parseInt(acumulado1);
                textViewPantalla2.setText(acumulado1);
                acumulado1 = "";
                textViewPantalla.setText(acumulado1);
                operacion = 3;
            }
        });
        buttondiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                valor1 = Integer.parseInt(acumulado1);
                textViewPantalla2.setText(acumulado1);
                acumulado1 = "";
                textViewPantalla.setText(acumulado1);
                operacion = 4;
            }
        });

        buttonequal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                valor2 = Integer.parseInt(acumulado1);
                switch (operacion){
                    case 1:
                        valorfinal=valor1+valor2;
                        break;
                    case 2:
                        valorfinal=valor1-valor2;
                        break;
                    case 3:
                        valorfinal=valor1*valor2;
                        break;
                    case 4:
                        valorfinal=valor1/valor2;
                        break;
                }
                acumulado1 = "";
                String respuesta = String.valueOf(valorfinal);
                textViewPantalla2.setText(acumulado1);
                textViewPantalla.setText(respuesta);

            }
        });
    }
}