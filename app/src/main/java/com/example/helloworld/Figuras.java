package com.example.helloworld;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.DisplayMetrics;
import android.view.View;

public class Figuras extends View {
    public Figuras(Context context) { super(context); }
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //FONDO BLANCO
        canvas.drawColor(Color.WHITE);
        //ahora lapiz
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setStrokeWidth(2);

        int ancho = getWidth();
        int alto = getHeight();
        int a = 500; // Alto
        int w = 8000; // Ancho

        float res = (float) a / w;
        float newa = a;
        float neww = w;

        int altoLimite = alto - 100;
        int anchoLimite = ancho - 100;
        //Definims proceso para escalar
        if (a > altoLimite || w > anchoLimite) {
            if (res > 1) { // Alto mayor q ancho
                while (newa > altoLimite || neww > anchoLimite) {
                    newa -= 1;
                    neww = newa / res;
                }
            } else { // Anch mayor que alto
                while (newa > altoLimite || neww > anchoLimite) {
                    neww -= 1;
                    newa = neww * res;
                }
            }
        }
        a = (int) newa;
        w = (int) neww;

        //dibujamos trangulo
        canvas.drawLine((float)(ancho * 0.05), (float)(alto * 0.05), (float)(ancho * 0.05), (float)(alto * 0.05 + a), paint);
        canvas.drawLine((float)(ancho * 0.05), (float)(alto * 0.05 + a), (float)(ancho * 0.05 + w), (float)(alto * 0.05 + a), paint);
        canvas.drawLine((float)(ancho * 0.05), (float)(alto * 0.05), (float)(ancho * 0.05 + w), (float)(alto * 0.05 + a), paint);

        //dibujamos rectangulo
        canvas.drawLine((float)(ancho * 0.05),(float)(alto * 0.5),(float)(ancho * 0.05),(float)((alto*0.5)+a),paint);
        canvas.drawLine((float)(ancho * 0.05),(float)(alto * 0.5),(float)((ancho * 0.05)+ w ),(float)(alto * 0.5),paint);
        canvas.drawLine((float)(ancho * 0.05), (float)(alto * 0.5 + a), (float)(ancho * 0.05 + w), (float)(alto * 0.5 + a), paint);
        canvas.drawLine((float)(ancho * 0.05 + w), (float)(alto * 0.5), (float)(ancho * 0.05 + w), (float)(alto * 0.5 + a), paint);
    }
}
