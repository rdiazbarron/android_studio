package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Preg1Activity extends AppCompatActivity {
    EditText editEntrada, editSalida;
    Button buttonValidar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preg1);

        editEntrada = findViewById(R.id.editEntrada);
        editSalida = findViewById(R.id.editSalida);
        buttonValidar = findViewById(R.id.buttonValidar);

        buttonValidar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String entrada = editEntrada.getText().toString();

                if (palabrasConMayusculaInicial(entrada)) {
                    editSalida.setText("correcto");
                } else {
                    editSalida.setText("incorrecto");
                }
            }
        });
    }

    private boolean palabrasConMayusculaInicial(String texto) {
        String[] palabras = texto.split("\\s+"); // Divide el texto en palabras usando espacio como delimitador
        for (String palabra : palabras) {
            if (palabra.isEmpty()) {
                continue;
            }
            char primerCaracter = palabra.charAt(0);
            String restoDePalabra = palabra.substring(1);

            if (!Character.isUpperCase(primerCaracter) || !restoDePalabra.equals(restoDePalabra.toLowerCase())) {
                return false;
            }
        }
        return true;
    }
}
