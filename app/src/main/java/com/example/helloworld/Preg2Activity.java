package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Preg2Activity extends AppCompatActivity {
    Button buttonRestar;
    EditText editMinuendo, editSustraendo, editDiferencia, editOperaciones;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preg2);
        buttonRestar = findViewById(R.id.buttonRestar);
        editMinuendo = findViewById(R.id.editMinuendo);
        editSustraendo = findViewById(R.id.editSustraendo);
        editDiferencia = findViewById(R.id.editDiferencia);
        editOperaciones = findViewById(R.id.editOperaciones);

        buttonRestar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int minuendo = Integer.parseInt(editMinuendo.getText().toString());
                int sustraendo = Integer.parseInt(editSustraendo.getText().toString());
                String diferencia = String.valueOf(minuendo - sustraendo);
                editDiferencia.setText(diferencia);
            }
        });
    }
}