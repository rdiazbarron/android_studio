package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class BDActivity extends AppCompatActivity {
    Button buttonSaveBD3 ,buttonShow; //botones para guardar y para mostrar los datos guardados
    BaseDatos2 baseDatosX; // se declara la base de datos que se usara en este activity
    SQLiteDatabase db;
    EditText editci,editnombres,editpaterno,editmaterno;
    TextView textdbnombres, textdbpaterno, textdbmaterno, textdbcarnet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_bdactivity);

        buttonSaveBD3 = findViewById(R.id.buttonSaveBD3);
        buttonShow = findViewById(R.id.buttonShow);

        editci = findViewById(R.id.editci);
        editnombres = findViewById(R.id.editnombres);
        editpaterno = findViewById(R.id.editpaterno);
        editmaterno = findViewById(R.id.editmaterno);


        textdbnombres = findViewById(R.id.textdbnombres);
        textdbpaterno = findViewById(R.id.textdbpaterno);
        textdbmaterno = findViewById(R.id.textdbmaterno);
        textdbcarnet = findViewById(R.id.textdbcarnet);
        //tengo BDPerros,version1
        baseDatosX = new BaseDatos2(BDActivity.this,"BDgatos1",null,1);
        //solamente cambiar nombre a la base de datos y listo, no hay necesidad de crear otra base de datos
        //ni cambiar de nombre a baseDatosX
        // si cambiamos la version a una superior, es como si formateasemos la BD y se vaciara, pero OJO
        //no se puede volver a una version anterior

        buttonSaveBD3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String ci2 = editci.getText().toString();
                int ci3 = Integer.parseInt(editci.getText().toString());
                double operacion = Math.exp(ci3);
                String res = String.valueOf(operacion);
                String nombres2 = editnombres.getText().toString();
                String paterno2 = editpaterno.getText().toString();
                String materno2 = editmaterno.getText().toString();
                try {
                    db = baseDatosX.getWritableDatabase();
                    if(ci2.isEmpty() || nombres2.isEmpty() || paterno2.isEmpty() || materno2.isEmpty()) {
                        Toast.makeText(BDActivity.this, "Complete todos los campos", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    db.execSQL("INSERT INTO personas(ci,paterno,materno,nombres)VALUES('"+ci2+"','"+paterno2+"','"+materno2+"','"+nombres2+"');");

                    editci.setText("");
                    editnombres.setText("");
                    editpaterno.setText("");
                    editmaterno.setText("");
                    Toast.makeText(BDActivity.this, res, Toast.LENGTH_SHORT).show();


                } catch (SQLiteException e) {
                    Log.e("DB_ERROR", "Database error", e);
                }finally {
                    if (db != null) {
                        db.close();
                    }
                }

            }
        });

        buttonShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String SQL = "SELECT * from personas;";
                db = baseDatosX.getReadableDatabase();

                Cursor cursor = db.rawQuery(SQL,null);
                if (cursor.moveToFirst()){
                    do{
                        String textdbcarnet1 =  cursor.getString(1);
                        textdbcarnet.setText(textdbcarnet1);

                        String textdbnombres1 = cursor.getString(2);
                        textdbnombres.setText(String.valueOf(textdbnombres1));

                        String textdbpaterno1 = cursor.getString(3);
                        textdbpaterno.setText(String.valueOf(textdbpaterno1));

                        String textdbmaterno1= cursor.getString(4);
                        textdbmaterno.setText(String.valueOf(textdbmaterno1));

                        Toast.makeText(BDActivity.this,textdbpaterno1,Toast.LENGTH_SHORT).show();

                    }while(cursor.moveToNext());
                    db.close();
                }else{
                    Toast.makeText(BDActivity.this, "No hay registros para mostrar.", Toast.LENGTH_SHORT).show();
                }
                cursor.close();
            }
        });
    }
}