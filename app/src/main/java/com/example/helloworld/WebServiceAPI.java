package com.example.helloworld;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

import java.util.List;



public interface WebServiceAPI {
    @GET("albums")
    Call<List<WSAlbums>> getAlbums();
    //holaa
    @GET("primer.php")
    Call<List<WSPersons>> getPersons();

    @GET("segundo.php")
    Call<List<WSCompras>> getCompras();

    @GET("ventas.php")
    Call<List<WSVentas>> getVentas();

    @GET("reserva.php")
    Call<List<WSReserva>> getReserva();

    //observables: recuperacion sea paralela, acba uno empieca otro

    @GET("primer.php")
    io.reactivex.Observable<List<WSPersons>> getPersonsObs();
    @GET("segundo.php")
    io.reactivex.Observable<List<WSCompras>> getComprasObs();

    @GET("ventas.php")
    io.reactivex.Observable<List<WSVentas>> getVentasObs();

    @GET("reserva.php")
    io.reactivex.Observable<List<WSReserva>> getReservaObs();

    @POST("tercero.php")
    Call<WSPersona> postPersona (@Body WSPersona modelPersona);
    //yo te envio datos y tu me envias

    @PUT("cuarto.php")
    Call<WSPersona> updatePersona(@Query("id") int id, @Body WSPersona persona);

    @DELETE("quinto.php")
    Call<Void> deletePersona(@Query("id") int id);

    @GET("sexto.php")
    Call<WSPersona> readPersona(@Query("id") int id);

}
